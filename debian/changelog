libpoe-filter-xml-perl (1.140700-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Martín Ferrari ]
  * Remove myself from Uploaders.

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 08:31:27 +0100

libpoe-filter-xml-perl (1.140700-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 14:15:13 +0100

libpoe-filter-xml-perl (1.140700-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  * New upstream release.
  * Drop (build) dependencies, which are not needed anymore.
  * Refresh spelling.patch (offset).
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 May 2014 14:47:27 +0200

libpoe-filter-xml-perl (1.102960-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Use new debhelper 7 short rules format
  * Standards-Version 3.9.1 (specifically refer to GPL-1+)
  * Use new 3.0 (quilt) source format
  * Bump debhelper compat to 8
  * Add myself to Uploaders and Copyright
  * Rewrite control description

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Email change: Ryan Niebur -> ryan@debian.org

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * Add /me to Uploaders.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Add a patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Dec 2012 15:32:29 +0100

libpoe-filter-xml-perl (0.38-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * Remove Florian Ragwitz from Uploaders (closes: #523286).

  [ Ryan Niebur ]
  * New upstream release
  * Add myself to Uploaders
  * Debian Policy 3.8.1
  * machine readable copyright format
  * debhelper 7
  * update dependencies

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 24 Apr 2009 21:13:23 -0700

libpoe-filter-xml-perl (0.33-1) unstable; urgency=low

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debianforum.de>); Florian Ragwitz <rafl@debianforum.de> moved
    to Uploaders.
  * Add debian/watch.

  [ Martín Ferrari ]
  * New upstream release.
  * debian/control: bumped Standards-Version (no changes), debhelper version
    to 5, correct versioned dependencies (build and run-time), corrected
    Description, added myself to Uploaders, moved Module::Build to
    Build-Depends.
  * debian/rules: refreshed from templates.
  * debian/copyright: new format, updated CP info, added packaging copyright.

 -- Martín Ferrari <tincho@debian.org>  Thu, 03 Jul 2008 04:31:06 -0300

libpoe-filter-xml-perl (0.31-0.2) unstable; urgency=low

  * Non-maintainer upload.
  * Declared missing build-dependencies (libmodule-builder-perl and
    libfilter-template-perl) and dependencies (libfilter-template-perl)
    I had missed in my previous NMU - Sorry! (Closes: #403462)

 -- Gunnar Wolf <gwolf@debian.org>  Sun, 17 Dec 2006 12:59:53 -0600

libpoe-filter-xml-perl (0.31-0.1) unstable; urgency=low

  * Non-Maintainer Upload by Gunnar Wolf, pkg-perl team
  * New upstream release (Closes: #392148)
  * Added an explicit version dependency on XML::SAX >= 0.14
  * Updated debian/rules, as the module now uses Module::Build instead
    of MakeMaker to be built

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 15 Dec 2006 13:07:12 -0600

libpoe-filter-xml-perl (0.27-1) unstable; urgency=low

  * Initial Release.

 -- Florian Ragwitz <rafl@debianforum.de>  Tue, 30 Aug 2005 16:26:35 +0200
